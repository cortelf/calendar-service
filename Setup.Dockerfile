FROM flyway/flyway

COPY flyway/calendar-service/migrations /flyway/sql

ENTRYPOINT flyway migrate