CREATE TABLE events (
    id serial NOT NULL,
    time timestamp without time zone NOT NULL,
    name TEXT NOT NULL,
    description TEXT NOT NULL,
    user_id TEXT NOT NULL,
    is_public BOOLEAN NOT NULL
);

CREATE INDEX events_time_idx ON events (time);