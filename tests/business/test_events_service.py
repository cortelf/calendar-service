from datetime import datetime
from unittest.mock import AsyncMock, Mock

from business.exceptions import NotFoundException, ForbiddenException
from business.models import CreateOrEditEvent, EventResponse
from business.mappers import MapCreateOrEditEventToEventContent, MapEventToEventResponse
from business.services.impl.events import EventsServiceImpl
import pytest

from persistence.entities.event import Event, EventContent

EXAMPLE_CREATE_OR_EDIT_REQUEST = CreateOrEditEvent(time=datetime.now(), name="test", description="testd", is_public=True)
EXAMPLE_EVENT_CONTENT = EventContent(time=datetime.now(), name="test", description="testd", is_public=True, user_id="q")
EXAMPLE_EVENT_RESPONSE = EventResponse(time=datetime.now(), name="test", description="testd", is_public=True, user_id="q", id=994)
EXAMPLE_EVENT = Event(time=datetime.now(), name="test", description="testd", is_public=True, user_id="q", id=994)


@pytest.fixture
def create_edit_mapper() -> MapCreateOrEditEventToEventContent:
    return Mock(return_value=EXAMPLE_EVENT_CONTENT)


@pytest.fixture
def event_mapper() -> MapEventToEventResponse:
    return Mock(return_value=EXAMPLE_EVENT_RESPONSE)


@pytest.mark.asyncio
async def test_create_event__all_ok__events_created_with_right_args(create_edit_mapper, event_mapper):
    dao = AsyncMock()
    service = EventsServiceImpl(dao, create_edit_mapper, event_mapper)
    user_id = "q"

    await service.create_event(user_id, EXAMPLE_CREATE_OR_EDIT_REQUEST)

    dao.create_event.assert_awaited_with(EXAMPLE_EVENT_CONTENT)


@pytest.mark.asyncio
async def test_put_event__not_exist_id__raise_NotFoundException(create_edit_mapper, event_mapper):
    dao = AsyncMock()
    dao.get_by_id.return_value = None
    service = EventsServiceImpl(dao, create_edit_mapper, event_mapper)

    with pytest.raises(NotFoundException):
        await service.put_event(0, "q", EXAMPLE_CREATE_OR_EDIT_REQUEST)


@pytest.mark.asyncio
async def test_put_event__another_user_id__raise_ForbiddenException(create_edit_mapper, event_mapper):
    dao = AsyncMock()
    dao.get_by_id.return_value = EXAMPLE_EVENT
    service = EventsServiceImpl(dao, create_edit_mapper, event_mapper)

    with pytest.raises(ForbiddenException):
        await service.put_event(0, "wrong", EXAMPLE_CREATE_OR_EDIT_REQUEST)


@pytest.mark.asyncio
async def test_put_event__all_ok__right_args_called(create_edit_mapper, event_mapper):
    dao = AsyncMock()
    service = EventsServiceImpl(dao, create_edit_mapper, event_mapper)
    dao.get_by_id.return_value = EXAMPLE_EVENT
    event_id = 994

    await service.put_event(event_id, "q", EXAMPLE_CREATE_OR_EDIT_REQUEST)

    dao.replace_event_content.assert_awaited_with(event_id, EXAMPLE_EVENT_CONTENT)


@pytest.mark.asyncio
async def test_delete_event__not_exist_id__raise_NotFoundException(create_edit_mapper, event_mapper):
    dao = AsyncMock()
    dao.get_by_id.return_value = None
    service = EventsServiceImpl(dao, create_edit_mapper, event_mapper)

    with pytest.raises(NotFoundException):
        await service.delete_event(0, "q")


@pytest.mark.asyncio
async def test_delete_event__another_user_id__raise_ForbiddenException(create_edit_mapper, event_mapper):
    dao = AsyncMock()
    dao.get_by_id.return_value = EXAMPLE_EVENT
    service = EventsServiceImpl(dao, create_edit_mapper, event_mapper)

    with pytest.raises(ForbiddenException):
        await service.delete_event(0, "wrong")

