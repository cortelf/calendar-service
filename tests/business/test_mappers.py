from datetime import datetime

import pytz

from business.mappers import (
    map_create_or_edit_event_to_event_content,
    map_event_to_event_response
)
from business.models import CreateOrEditEvent
from persistence.entities.event import Event
import pytest


def test_map_create_or_edit_event_to_event_content():
    original = CreateOrEditEvent(time=datetime.now(), name="test", description="testd", is_public=True)
    user_id = "q"

    response = map_create_or_edit_event_to_event_content(user_id, original)

    assert response.name == original.name
    assert response.user_id == user_id
    assert response.time == original.time
    assert response.description == original.description
    assert response.is_public == original.is_public


def test_map_event_to_event_response():
    original = Event(time=datetime.now(), name="test", description="testd", is_public=True, user_id="q", id=994)

    response = map_event_to_event_response(original)

    assert response.name == original.name
    assert response.user_id == original.user_id
    assert response.time == original.time.astimezone(pytz.utc)
    assert response.description == original.description
    assert response.is_public == original.is_public
    assert response.id == original.id
