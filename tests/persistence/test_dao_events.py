import asyncio
from asyncio import WindowsSelectorEventLoopPolicy

import psycopg
import os
import pytest_asyncio

from persistence.dao.impl.events import EventsDaoImpl
import pytest

from persistence.entities.event import EventContent
from datetime import datetime, timedelta
import dataclasses

EVENT_CONTENT = EventContent(time=datetime.now(), is_public=True, description="test", user_id="111", name="1111")


@pytest_asyncio.fixture
async def connection():
    async with await psycopg.AsyncConnection.connect(
            os.getenv("POSTGRES_TEST_CONNECTION")) as conn:
        yield conn
        await conn.execute("TRUNCATE events;")


@pytest_asyncio.fixture
async def create_dao(connection):
    return EventsDaoImpl(connection)


@pytest.mark.asyncio
async def test__create_event__all_ok__id_returned(create_dao):
    res = await create_dao.create_event(
        EVENT_CONTENT)

    assert res.id is not None


@pytest.mark.asyncio
async def test__get_by_id__wrong_id__none_returned(create_dao):
    res = await create_dao.get_by_id(99999)

    assert res is None


@pytest.mark.asyncio
async def test__get_by_id__id_after_create__is_not_none(create_dao):
    created_event = await create_dao.create_event(
        EVENT_CONTENT)
    res = await create_dao.get_by_id(created_event.id)

    assert res is not None


@pytest.mark.asyncio
async def test__delete_event__id_after_create__get_by_id_returns_none(create_dao):
    created_event = await create_dao.create_event(
        EVENT_CONTENT)
    await create_dao.delete_event(created_event.id)
    res = await create_dao.get_by_id(created_event.id)

    assert res is None


@pytest.mark.asyncio
async def test__delete_event__wrong_id__does_nothing(create_dao):
    await create_dao.delete_event(99999)


@pytest.mark.asyncio
async def test__replace_event_content__replace_after_create__get_by_id_returns_right_value(create_dao):
    new_name = "replaced"

    created_event = await create_dao.create_event(
        EVENT_CONTENT)
    await create_dao.replace_event_content(created_event.id, dataclasses.replace(EVENT_CONTENT, name=new_name))
    res = await create_dao.get_by_id(created_event.id)

    assert res.name == new_name


@pytest.mark.asyncio
@pytest.mark.parametrize('date,user_id,is_public', [
    (datetime(2023, 10, 1), "q1", True),
    (datetime(2023, 10, 1), "q1", False),
    (datetime(2023, 10, 1), "q2", True),

    (datetime(2023, 10, 5), "q1", True),
    (datetime(2023, 10, 5), "q1", False),
    (datetime(2023, 10, 5), "q2", True),

    (datetime(2023, 10, 31), "q1", True),
    (datetime(2023, 10, 31), "q1", False),
    (datetime(2023, 10, 31), "q2", True),
])
async def test__get_events_for_user_by_date_range__right_date__return_result(create_dao, date, user_id, is_public):
    requested_user = "q1"
    date_from = datetime(2023, 10, 1)
    date_to = datetime(2023, 11, 1)
    content = dataclasses.replace(EVENT_CONTENT, time=date, user_id=user_id, is_public=is_public)

    created_event = await create_dao.create_event(content)
    res = await create_dao.get_events_for_user_by_date_range(requested_user, date_from, date_to)

    assert res[0].id == created_event.id


@pytest.mark.asyncio
@pytest.mark.parametrize('date,user_id,is_public', [
    (datetime(2023, 11, 1), "q1", True),
    (datetime(2023, 11, 1), "q1", False),
    (datetime(2023, 11, 1), "q2", False),

    (datetime(2023, 10, 1), "q2", False),
    (datetime(2023, 10, 5), "q2", False),
    (datetime(2023, 10, 31), "q2", False),

    (datetime(2023, 9, 30), "q1", True),
    (datetime(2023, 9, 30), "q1", False),
    (datetime(2023, 9, 30), "q2", False),
])
async def test__get_events_for_user_by_date_range__wrong_date__not_return_result(create_dao, date, user_id, is_public):
    requested_user = "q1"
    date_from = datetime(2023, 10, 1)
    date_to = datetime(2023, 11, 1)
    content = dataclasses.replace(EVENT_CONTENT, time=date, user_id=user_id, is_public=is_public)

    await create_dao.create_event(content)
    res = await create_dao.get_events_for_user_by_date_range(requested_user, date_from, date_to)

    assert len(res) == 0


@pytest.mark.asyncio
async def test__get_events_for_user_by_date_range__right_items_right_sorting_order(create_dao):
    requested_user = "q1"
    date_from = datetime(2023, 10, 1)
    date_to = datetime(2023, 11, 1)
    events_to_create = [
        dataclasses.replace(EVENT_CONTENT, time=date_from + timedelta(days=1), name="1"),
        dataclasses.replace(EVENT_CONTENT, time=date_from + timedelta(days=3), name="3"),
        dataclasses.replace(EVENT_CONTENT, time=date_from + timedelta(days=2), name="2")
    ]

    for event in events_to_create:
        await create_dao.create_event(event)
    res = await create_dao.get_events_for_user_by_date_range(requested_user, date_from, date_to)

    assert res[0].name == "1" and res[1].name == "2" and res[2].name == "3"

if os.name == "nt":
    asyncio.set_event_loop_policy(WindowsSelectorEventLoopPolicy())
