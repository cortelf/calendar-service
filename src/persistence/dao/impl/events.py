from typing import List, Optional

from psycopg import AsyncConnection
from psycopg.rows import class_row
from datetime import datetime

from persistence.dao.events import EventsDao
from persistence.entities.event import Event, EventContent


class EventsDaoImpl(EventsDao):
    _connection: AsyncConnection

    def __init__(self, connection: AsyncConnection):
        self._connection = connection

    async def create_event(self, content: EventContent) -> Event:
        async with self._connection.cursor(row_factory=class_row(Event)) as cur:
            query = await cur.execute(
                "INSERT INTO events (time, name, description, user_id, is_public) "
                "VALUES (%s, %s, %s, %s, %s) RETURNING *",
                (
                    content.time,
                    content.name,
                    content.description,
                    content.user_id,
                    content.is_public,
                ),
            )

            res = await query.fetchone()
            await self._connection.commit()
        return res

    async def delete_event(self, event_id: int) -> None:
        async with self._connection.cursor() as cur:
            await cur.execute("DELETE FROM events WHERE id = %s", (event_id,))
            await self._connection.commit()

    async def replace_event_content(
        self, event_id: int, new_content: EventContent
    ) -> None:
        async with self._connection.cursor() as cur:
            await cur.execute(
                "UPDATE events "
                "SET time = %s, name = %s, description = %s, user_id = %s, is_public = %s "
                "WHERE id = %s",
                (
                    new_content.time,
                    new_content.name,
                    new_content.description,
                    new_content.user_id,
                    new_content.is_public,
                    event_id,
                ),
            )
            await self._connection.commit()

    async def get_events_for_user_by_date_range(
        self, user_id: str, date_from: datetime, date_to: datetime
    ) -> List[Event]:
        async with self._connection.cursor(row_factory=class_row(Event)) as cur:
            query = await cur.execute(
                "SELECT * FROM events WHERE (user_id = %s OR is_public = TRUE) "
                "AND time >= %s AND time < %s ORDER BY time",
                (user_id, date_from, date_to),
            )
            res = await query.fetchall()
            await self._connection.commit()
        return res

    async def get_by_id(self, event_id: int) -> Optional[Event]:
        async with self._connection.cursor(row_factory=class_row(Event)) as cur:
            query = await cur.execute("SELECT * FROM events WHERE id = %s",
                                      (event_id,))
            res = await query.fetchone()
            await self._connection.commit()
        return res
