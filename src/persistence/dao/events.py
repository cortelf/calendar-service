from abc import ABC, abstractmethod
from typing import List, Optional, Awaitable
from datetime import datetime

from persistence.entities.event import EventContent, Event


class EventsDao(ABC):
    @abstractmethod
    def create_event(self, content: EventContent) -> Awaitable[Event]:
        ...

    @abstractmethod
    def delete_event(self, event_id: int) -> Awaitable[None]:
        ...

    @abstractmethod
    def replace_event_content(
        self, event_id: int, new_content: EventContent
    ) -> Awaitable[None]:
        ...

    @abstractmethod
    def get_events_for_user_by_date_range(
        self, user_id: str, date_from: datetime, date_to: datetime
    ) -> Awaitable[List[Event]]:
        ...

    @abstractmethod
    def get_by_id(self, event_id: int) -> Awaitable[Optional[Event]]:
        ...
