from dataclasses import dataclass
from datetime import datetime


@dataclass(frozen=True)
class EventContent:
    time: datetime
    name: str
    description: str
    user_id: str
    is_public: bool


@dataclass(frozen=True)
class Event(EventContent):
    id: int
