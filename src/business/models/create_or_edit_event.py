from datetime import datetime
from pydantic import BaseModel


class CreateOrEditEvent(BaseModel):
    time: datetime
    name: str
    description: str
    is_public: bool
