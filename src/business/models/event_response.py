from datetime import datetime
from pydantic import BaseModel


class EventResponse(BaseModel):
    id: int
    time: datetime
    name: str
    description: str
    user_id: str
    is_public: bool
