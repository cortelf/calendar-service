from datetime import datetime
from typing import List

from business.exceptions import NotFoundException, ForbiddenException
from business.models import CreateOrEditEvent, EventResponse
from business.services.events import EventsService
from persistence.entities.event import Event
from persistence.dao.events import EventsDao
from business.mappers import (
    MapCreateOrEditEventToEventContent,
    MapEventToEventResponse,
)


class EventsServiceImpl(EventsService):
    _events_dao: EventsDao
    _create_edit_mapper: MapCreateOrEditEventToEventContent
    _event_mapper: MapEventToEventResponse

    def __init__(self,
                 events_dao: EventsDao,
                 create_edit_mapper: MapCreateOrEditEventToEventContent,
                 event_mapper: MapEventToEventResponse):
        self._events_dao = events_dao
        self._create_edit_mapper = create_edit_mapper
        self._event_mapper = event_mapper

    async def create_event(
        self, user_id: str, content: CreateOrEditEvent
    ) -> EventResponse:
        event = await self._events_dao.create_event(
            self._create_edit_mapper(user_id, content)
        )
        return self._event_mapper(event)

    async def delete_event(self, event_id: int, user_id: str) -> None:
        event = await self._get_by_id(event_id)
        if event.user_id != user_id:
            raise ForbiddenException()

        return await self._events_dao.delete_event(event_id)

    async def put_event(
        self, event_id: int, user_id: str, new_content: CreateOrEditEvent
    ) -> EventResponse:
        event = await self._get_by_id(event_id)
        if event.user_id != user_id:
            raise ForbiddenException()

        await self._events_dao.replace_event_content(
            event_id, self._create_edit_mapper(user_id, new_content)
        )
        return self._event_mapper(await self._get_by_id(event_id))

    async def get_events_for_user_by_date_range(
        self, user_id: str, date_from: datetime, date_to: datetime
    ) -> List[EventResponse]:
        events = await self._events_dao.get_events_for_user_by_date_range(
            user_id, date_from, date_to
        )
        return [self._event_mapper(e) for e in events]

    async def _get_by_id(self, event_id: int) -> Event:
        event = await self._events_dao.get_by_id(event_id)
        if event is None:
            raise NotFoundException()
        return event
