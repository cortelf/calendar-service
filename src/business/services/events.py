from abc import ABC, abstractmethod
from typing import List, Awaitable

from business.models import CreateOrEditEvent, EventResponse
from datetime import datetime


class EventsService(ABC):
    @abstractmethod
    def create_event(
        self, user_id: str, content: CreateOrEditEvent
    ) -> Awaitable[EventResponse]:
        ...

    @abstractmethod
    def delete_event(self, event_id: int, user_id: str) -> Awaitable[None]:
        ...

    @abstractmethod
    def put_event(
        self, event_id: int, user_id: str, new_content: CreateOrEditEvent
    ) -> Awaitable[EventResponse]:
        ...

    @abstractmethod
    def get_events_for_user_by_date_range(
        self, user_id: str, date_from: datetime, date_to: datetime
    ) -> Awaitable[List[EventResponse]]:
        ...

    # @abstractmethod
    # def get_by_id(self, event_id: int) -> Awaitable[Event]:
    #     ...
