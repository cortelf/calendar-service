class NotFoundException(Exception):
    pass


class ForbiddenException(Exception):
    pass
