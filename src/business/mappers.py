from typing import Callable

import pytz

from business.models import CreateOrEditEvent, EventResponse
from persistence.entities.event import EventContent, Event


MapCreateOrEditEventToEventContent = Callable[[str, CreateOrEditEvent], EventContent]
MapEventToEventResponse = Callable[[Event], EventResponse]


def map_create_or_edit_event_to_event_content(
    user_id: str, original: CreateOrEditEvent
) -> EventContent:
    return EventContent(
        time=original.time,
        name=original.name,
        user_id=user_id,
        description=original.description,
        is_public=original.is_public,
    )


def map_event_to_event_response(original: Event) -> EventResponse:
    return EventResponse(
        id=original.id,
        is_public=original.is_public,
        user_id=original.user_id,
        description=original.description,
        name=original.name,
        time=original.time.astimezone(pytz.utc),
    )
