from blacksheep import Response

from business.exceptions import NotFoundException, ForbiddenException


async def not_found_exception_handler(self, request, exc: NotFoundException):
    return Response(404)


async def forbidden_exception_handler(self, request, exc: ForbiddenException):
    return Response(403)
