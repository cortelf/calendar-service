from psycopg import AsyncConnection
from psycopg_pool import AsyncConnectionPool
from rodi import GetServiceContext


async def database_connection_middleware(request, handler, pool: AsyncConnectionPool):
    scope: GetServiceContext = request.services_context
    async with pool.connection() as conn:
        scope.scoped_services[AsyncConnection] = conn
        return await handler(request)
