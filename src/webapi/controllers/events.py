from datetime import datetime

from blacksheep import auth, FromQuery
from blacksheep.server.controllers import ApiController, get, post, put, delete
from guardpost import User

from business.models import CreateOrEditEvent
from business.services.events import EventsService


class EventsController(ApiController):
    _service: EventsService

    def __init__(self, service: EventsService):
        self._service = service

    @classmethod
    def route(cls) -> str:
        return "events"

    @auth("protected")
    @post()
    async def post_event(self, user: User, body: CreateOrEditEvent):
        return await self._service.create_event(user_id=user.sub, content=body)

    @auth("protected")
    @put("{int:event_id}")
    async def put_event(
        self, user: User, event_id: int, body: CreateOrEditEvent
    ):
        return await self._service.put_event(
            user_id=user.sub, event_id=event_id, new_content=body
        )

    @auth("protected")
    @delete("{int:event_id}")
    async def delete_event(self, user: User, event_id: int):
        await self._service.delete_event(user_id=user.sub, event_id=event_id)
        return {}

    @auth("protected")
    @get()
    async def get_events(
        self,
        user: User,
        date_from: FromQuery[datetime],
        date_to: FromQuery[datetime],
    ):
        return await self._service.get_events_for_user_by_date_range(
            user_id=user.sub, date_from=date_from.value, date_to=date_to.value
        )
