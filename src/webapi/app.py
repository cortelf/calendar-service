
from blacksheep import Application
from blacksheep.server.authentication.jwt import JWTBearerAuthentication
from blacksheep.server.di import dependency_injection_middleware
from blacksheep.server.openapi.v3 import OpenAPIHandler
from guardpost import Policy
from guardpost.common import AuthenticatedRequirement
from openapidocs.v3 import Info
from psycopg import AsyncConnection
from psycopg_pool import AsyncConnectionPool

from business.exceptions import NotFoundException, ForbiddenException
from business.mappers import (
    MapCreateOrEditEventToEventContent,
    map_event_to_event_response,
    map_create_or_edit_event_to_event_content,
    MapEventToEventResponse
)

from business.services.events import EventsService
from business.services.impl.events import EventsServiceImpl
from persistence.dao.events import EventsDao
from persistence.dao.impl.events import EventsDaoImpl
from webapi.config import settings

from webapi.controllers.events import EventsController  # noqa: F401
from webapi.exception_handlers import (
    not_found_exception_handler,
    forbidden_exception_handler,
)
from webapi.middlewares.database_connection import database_connection_middleware

app = Application()
docs = OpenAPIHandler(info=Info(title="Calendar service API", version="0.0.1"))
docs.bind_app(app)

app.services.add_singleton_by_factory(
    lambda _: AsyncConnectionPool(settings.CONNECTION_STRING), AsyncConnectionPool
)

# hack for rodi
app.services.add_scoped_by_factory(
    lambda a: a.provider[AsyncConnectionPool].connection(), AsyncConnection
)

app.services.add_scoped(EventsDao, EventsDaoImpl)

app.services.add_singleton_by_factory(
    lambda _: map_create_or_edit_event_to_event_content, MapCreateOrEditEventToEventContent)
app.services.add_singleton_by_factory(
    lambda _: map_event_to_event_response, MapEventToEventResponse)

app.services.add_scoped(EventsService, EventsServiceImpl)

app.use_authentication().add(
    JWTBearerAuthentication(
        authority=settings.JWT_AUTHORITY,
        valid_audiences=[settings.JWT_VALID_AUDIENCE],
        valid_issuers=[settings.JWT_VALID_ISSUER],
    )
)

app.use_cors(
    allow_methods="*",
    allow_origins="*",
    allow_headers="* Authorization",
    max_age=300,
)

authorization = app.use_authorization()

authorization += Policy("protected", AuthenticatedRequirement())

app.middlewares.append(dependency_injection_middleware)
app.middlewares.append(database_connection_middleware)

app.exceptions_handlers[NotFoundException] = not_found_exception_handler
app.exceptions_handlers[ForbiddenException] = forbidden_exception_handler
